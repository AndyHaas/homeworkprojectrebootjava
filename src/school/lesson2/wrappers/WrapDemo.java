package school.lesson2.wrappers;

public class WrapDemo {


	public static void main(String[] args) {
		
	      boolean boo = false;
	      Boolean wboo = new Boolean(boo);
	      wboo = boo;
	      	      
	      System.out.println("boolean variable's value: " + wboo); 
	      
	      byte b = 2;
	      Byte wbyte = new Byte(b);
	      System.out.println("byte variable's value: " + wbyte); 
	      
	      short s = 4;
	      Short wshort = new Short(s);
	      System.out.println("short variable's value: " + wshort); 
	      
	      int i = 16;
	      Integer wint = new Integer(i);
	      System.out.println("int variable's value: " + wint); 
	      
	      long l = 123;
	      Long wlong = new Long(l);
	      System.out.println("long variable's value: " + wlong); 
	      
	      float f = 12.34f;
	      Float wfloat = new Float(f);
	      System.out.println("float variable's value: " + wfloat); 
	      
	      double d = 12.56d;
	      Double wdouble = new Double(d);
	      System.out.println("double variable's value: " + wdouble); 
     }
}