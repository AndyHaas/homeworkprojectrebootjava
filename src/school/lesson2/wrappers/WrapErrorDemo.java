package school.lesson2.wrappers;

public class WrapErrorDemo {

   public static void main(String[] args) {

      System.out.println("Compare 50 and 100 : " + compare(50, 100)); // 1
      System.out.println("Compare 100 and 50 : " + compare(100, 50));// 1
      System.out.println("Compare 50 and 50 : " + compare(50, 50));  // 0
      System.out.println("Compare 200 and 200 : " + compare(200, 200));//1  

      }

   public static int compare(Integer  first, Integer second) {

      return first < second ? -1 : (first == second ? 0 : 1);
   }
}
