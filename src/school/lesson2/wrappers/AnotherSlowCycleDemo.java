package school.lesson2.wrappers;

public class AnotherSlowCycleDemo {

   public static void main(String[] args) {

      long time = System.currentTimeMillis();

      Long sum = 0L;

      for (long i = 0; i <= 100000000L; i++) {

         sum += i;
      }

      time = System.currentTimeMillis() - time;
      System.out.println("It took " + time + " milliseconds to execute this cycle");
      System.out.println("Sum is: " + sum);
   }
}
