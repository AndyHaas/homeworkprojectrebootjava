package school.lesson2.primitives;

public class ResultOverflowDemo {

   public static void main(String[] args) {

      long yearMilliseconds = 1000 * 365 * 24 * 60 * 60;
      System.out.println("Wrong number of milliseconds per year: " + yearMilliseconds);

      yearMilliseconds = 1000L * 365 * 24 * 60 * 60 ;
      System.out.println("Correct number of milliseconds per year: " + yearMilliseconds);
   }
}
