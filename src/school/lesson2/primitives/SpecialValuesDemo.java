package school.lesson2.primitives;

public class SpecialValuesDemo {

   public static void main(String[] args) {

      double max = Double.MAX_VALUE;
      double min = Double.MIN_VALUE;

      System.out.println("Maximum double value approximately is: " + max);
      System.out.println("Minimum positive double value approximately is: " + min);
      System.out.println("Positive infinity is: " + max * 2);
      System.out.println("Positive zero is: " + min / 2);
      System.out.println("Negative infinity is: " + (-max * 2));
      System.out.println("Negative zero is: " + (-min / 2));
      
      System.out.println("Not a number: " + Math.sqrt(-1));
   }
}
