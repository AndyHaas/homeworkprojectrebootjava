package school.lesson2.primitives;

public class SilentOverflowDemo {

   public static void main(String[] args) {

      int posint = 2147483647;

      System.out.println("number is: " + posint);
      System.out.println("number + 1 is: " + (posint + 1));

      System.out.println();

      int negint = -2147483648;

      System.out.println("number is: " + negint);
      System.out.println("number - 1 is: " + (negint - 1));
   }
}
