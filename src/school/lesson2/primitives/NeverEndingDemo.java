package school.lesson2.primitives;

public class NeverEndingDemo {

   public static void main(String[] args) throws Exception{

     for (double x = 0; x != 10; x += 0.1) {
      
        System.out.println(x);
        Thread.sleep(50);
     }
   }
}
