package school.lesson2.primitives;

public class TruthTableDemo {

   public static void main(String[] args) {

      System.out.println("R\tS\tAND\tOR\tXOR\tNOT");
      printLine(true, true);
      printLine(true, false);
      printLine(false, true);
      printLine(false, false);
   }

   static void printLine(boolean l, boolean r) {

      System.out.println(l + "\t" + r + "\t" + (l & r) + "\t" + (l | r) + "\t"
            + (l ^ r) + "\t" + (!l));
   }
}