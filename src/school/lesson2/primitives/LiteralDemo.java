package school.lesson2.primitives;


public class LiteralDemo {

   public static void main(String[] args) {

      String name = "Harry Hacker";
      char gender = 'm';
      boolean isMarried = true;
      byte numChildren = 2;
      short yearOfBirth = 1987;
      int salary = 30000;
      long netAsset = 8234567890L;
      double weight = 88.88;
      float gpa = 4.58f;

      System.out.println("Name: " + name);
      System.out.println("Gender: " + gender);
      System.out.println("Is married: " + isMarried);
      System.out.println("Number of children: " + numChildren);
      System.out.println("Year of birth: " + yearOfBirth);
      System.out.println("Salary: " + salary);
      System.out.println("Net Asset: " + netAsset);
      System.out.println("Weight: " + weight);
      System.out.println("GPA: " + gpa);
   }
}
