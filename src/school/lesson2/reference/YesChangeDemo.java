package school.lesson2.reference;

public class YesChangeDemo {

   public static void main(String[] args) {

      Employee alice = new Employee("Alice", 20000);

      System.out.println("Employee Alice: name = " + alice.name + ", salary = "
            + alice.salary);

      DoubleSalary(alice);

      System.out.println("Employee Alice: name = " + alice.name + ", salary = "
            + alice.salary);
   }

   public static void DoubleSalary(Employee a) {

      a.raiseSalary(100);
   }
}
