package school.lesson2.reference;

public class ChangeRefDemo {

   public static void main(String[] args) {

      Employee bob = new Employee("Robert", 20000);
      Employee robert = bob;

      System.out.println("Object employee: name = " + bob.name + ", salary = "
            + bob.salary);

      System.out.println("Doubling salary ...");
      
      robert.raiseSalary(100);
      
      System.out.println("Object employee: name = " + bob.name + ", salary = "
            + bob.salary);
   
   }
}
