package school.lesson2.reference;

public class NoRefDemo {

   public static void main(String[] args) {

      Employee bob = new Employee("Robert", 20000);
      
      System.out.println("Object employee: name = " + bob.name + ", salary = "
            + bob.salary);

      System.out.println("Making the only reference null ...");
      bob = null;
      
      System.out.println("Reference to Robert is now: " + bob);
         
   }
}
