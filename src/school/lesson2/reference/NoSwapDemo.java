package school.lesson2.reference;

public class NoSwapDemo {

   public static void main(String[] args) {
      
      Employee alice = new Employee("Alice", 20000);
      Employee bob = new Employee("Robert", 30000);
     
      System.out.println("Employee Alice: name = " + alice.name + ", salary = "
            + alice.salary);
  
      System.out.println("Employee Bob: name = " + bob.name + ", salary = "
            + bob.salary);
    
      swap(alice, bob);
      
      System.out.println("Employee Alice: name = " + alice.name + ", salary = "
            + alice.salary);
  
      System.out.println("Employee Bob: name = " + bob.name + ", salary = "
            + bob.salary);
   }

   public static void swap(Employee a, Employee b) {
      
      Employee temp;
      
      temp = a;
      a = b;
      b = temp;
   }
}
