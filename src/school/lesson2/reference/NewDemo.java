package school.lesson2.reference;

public class NewDemo {

   public static void main(String[] args) {

      Employee bob;
      bob = new Employee("Robert", 20000);
      
      Employee alice = new Employee("Alice", 10000);

      System.out.println("Object employee: name = " + bob.name + ", salary = "
            + bob.salary);
      System.out.println("Object employee: name = " + alice.name + ", salary = "
            + alice.salary);
   }
}