package school.lesson2.reference;

public class AssignDemo {

   public static void main(String[] args) {

      Employee bob = new Employee("Robert", 20000);
      Employee robert = bob;

      System.out
            .println("Names and salaries are equal: "
                  + ((robert.name.equals(bob.name)) && (robert.salary == bob.salary)));
      System.out.println("References are equal: " + (robert == bob));
   }
}
