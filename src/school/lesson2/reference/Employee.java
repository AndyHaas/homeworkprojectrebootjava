package school.lesson2.reference;

class Employee {
   
   int salary;
   String name;
   
   Employee(String name, int salary) { 
      this.name = name;
      this.salary = salary;
   }
   
   public void raiseSalary(int byPercent) {
      int raise = salary * byPercent / 100;
      salary += raise;
   }
}