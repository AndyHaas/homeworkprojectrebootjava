package school.lesson2.control;

import java.util.Scanner;

public class IfElseDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
     
      System.out.println("Enter your sales:");
      double yourSales = in.nextDouble();
    
      System.out.println("Enter your target:");
      double target = in.nextDouble();
   
      in.close();
      
      String performance;
      double bonus;
      
      if (yourSales >= target) {
         performance = "Satisfactory";
         bonus = 10 + 0.05 * (yourSales - target);
      } else {
         performance = "Unsatisfactory";
         bonus = 0;
      }
      System.out.println("Your performance is " + performance
            + "\nYour bonus: " + bonus);
   }
}
