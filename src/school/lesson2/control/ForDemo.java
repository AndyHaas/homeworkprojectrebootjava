package school.lesson2.control;

import java.util.Scanner;

public class ForDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
    
      System.out.println("Enter the number of years till retirement:");
      int  years = in.nextInt();
      
      System.out.println("Enter your retirement payment:");
      double payment = in.nextDouble();
      
      System.out.println("Enter interest rate:");
      double interestRate = in.nextDouble();

      in.close();
      
      double balance = 0;
      
      for(byte y = 0; y < years ; y++) {
         
         balance += payment;
         double interest = balance * interestRate / 100;
         balance += interest;
         System.out.println("After year " + (y+1) + " your balance is: " + balance);
      }
   }
}
