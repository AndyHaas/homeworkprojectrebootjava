package school.lesson2.control;

import java.util.Scanner;

public class WhileDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      
      System.out.println("Enter your retirement goal:");
      double goal = in.nextDouble();
      
      System.out.println("Enter your retirement sallary:");
      double payment = in.nextDouble();
      
      System.out.println("Enter interest rate:");
      double interestRate = in.nextDouble();

      in.close();
      
      double balance = 0;
      int years = 0;

      while (balance < goal) {
         balance += payment;
         double interest = balance * interestRate / 100;
         balance += interest;
         System.out.println("Your balance is: " + balance);

         years++;
      }
      System.out.println("Your will be able to retire in " + years + " years\n");
   }
}
