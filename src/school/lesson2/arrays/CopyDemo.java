package school.lesson2.arrays;

import java.util.Arrays;

public class CopyDemo {

   public static void main(String[] args) {

      char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a',
            't', 'e', 'd' };
      char[] copyTo = new char[7];

      System.out.println("Initial array: ");
      System.out.println(copyFrom);

      System.out.println("Copying Array using arraycopy...");
      System.arraycopy(copyFrom, 2, copyTo, 0, 7);
      System.out.println(copyTo);

      System.out.println("Copying Array using copyOfRange...");
      copyTo = Arrays.copyOfRange(copyFrom, 2, 9);
      System.out.println(copyTo);
   }
}
