package school.lesson2.arrays;

public class DeclDemo {

   public static void main(String[] args) {

      int[] anArray;
      anArray = new int[10];

      for (int i = 0; i < 10; i++) {

         anArray[i] = 100 * i;
      }

      for (int i = 0; i < 10; i++) {

         System.out.println(anArray[i]);
      }
   }
}
