package school.lesson2.arrays;

import java.util.Arrays;

public class EqualsDemo {

    public static void main(String[] args) {

        int[] one = { 1, 2, 3, 4, 5 };
        int[] two = { 1, 2, 3, 4, 5 };
        two = one;


        System.out.println("Array one: " + Arrays.toString(one));
        System.out.println("Array two: " + Arrays.toString(two));

        System.out.println("Comparing arrays one and two.....");

        System.out.println("Using equals(): " + one.equals(two));
        System.out.println("Using Arrays.equals(): " + Arrays.equals(one,two));
    }
}
