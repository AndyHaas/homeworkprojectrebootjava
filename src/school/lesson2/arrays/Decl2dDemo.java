package school.lesson2.arrays;


public class Decl2dDemo {

   public static void main(String[] args) {

      int[][] anArray = new int[3][];

      anArray[0] = new int[] { 0, 1, 2 };
      anArray[1] = new int[] { 3, 4, 5, 6, 7 };
      anArray[2] = new int[] { 8, 9, 10 };

      for (int i = 0; i < anArray.length; i++) {
         for (int j = 0; j < anArray[i].length; j++) {

            System.out.print(anArray[i][j] + " ");
         }
         System.out.println();
      }
   }
}
