package school.lesson2.arrays;

import java.util.Arrays;

public class InitDemo {

	
	public static void main(String[] args) {
		
		
		int[] anArray = { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };

		anArray = new int[]{ 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100 };

		System.out.println(Arrays.toString(anArray));
	}
}
