package school.lesson2.arrays;

import java.util.Arrays;

public class SortDemo {

   public static void main(String[] args) {

      int[] anArray = { 900, 200, 1000, 700, 500, 600, 400, 800, 300, 100 };

      for (int i = 0; i < 10; i++) {

         System.out.print(anArray[i] + " ");
      }
      System.out.println();

      System.out.println("Sorting Array...");
      Arrays.sort(anArray);

      for (int a : anArray) {

         System.out.print(a + " ");
      }
      System.out.println();
   }
}
