package school.lesson4.encoding;

import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

public class EncodingDemo {

   public static void main(String[] args) throws UnsupportedEncodingException,
         IOException {

      System.out.println("Default Charset=" + Charset.defaultCharset());
      System.out.println("������ ���!");

      OutputStreamWriter writer = new OutputStreamWriter(System.out, "UTF-8");
      writer.write("Hello World! ������ ���!");
      writer.flush();
      System.out.println("\u20AC".getBytes("UTF-8").length);
      System.out.println("\u00A3".getBytes("UTF-8").length);
      System.out.println("\u0053".getBytes("UTF-8").length);
      //0163      
      //
      //0128
      System.out.println("\u00A2");
    

      System.out.println("\u0000\u0000\u0000".getBytes("UTF-16").length);
      System.out.println(Arrays.toString("\u0000\u0000\u0000".getBytes("UTF-16")));
      System.out.println(Arrays.toString("\ufeff".getBytes("UTF-16")));
    
      System.out.println("To travel from Leicester to Loughborough it will cost Java \u2122 \u00A3 2.50");
   }
}
