package school.lesson4.encoding;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class EncodingDemo1 {

   public static void main(String[] args) throws IOException {

      FileOutputStream fop;

      fop = new FileOutputStream("newfile.txt");

      // OutputStreamWriter osw = new OutputStreamWriter(fop, "ISO-8859-5");
      OutputStreamWriter osw = new OutputStreamWriter(fop, "UTF-16");

      osw.write("Привет Мир!");
      osw.close();
      System.out.println("Done");
   }
}