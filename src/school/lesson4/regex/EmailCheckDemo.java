package school.lesson4.regex;

import java.util.regex.Pattern;

public class EmailCheckDemo {

   public static void main(String[] args) {

      final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+"
            + "(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
            + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

      String[] emails = { "hello...world@yahoo.com", "h12345@@mail.com",
            "sidorov12345@gmail.com", "ivanov@diasoft.ru", "dedmoroz@mail.ru" };

      for (String email : emails) {
         if (Pattern.matches(EMAIL_PATTERN, email)) {
            System.out.println(email + " is a valid email address");
         } else {
            System.out.println(email + " is not a valid email address");
         }
      }
   }
}
