package school.lesson4.regex;

import java.util.regex.Pattern;

public class DateCheckDemo {

   public static void main(String[] args) {

      final String DATE_PATTERN = "^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)$";

      String[] dates = {"31/12/2012", "22/12/2012", "12/12/2012", "Hello", "12/15/2012"};

      for (String date : dates) {
         if (Pattern.matches(DATE_PATTERN, date)) {
            System.out.println(date + " is a valid date");
         } else {
            System.out.println(date + " is not a valid date");
         }
      }
   }
}
