package school.lesson4.regex;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class MatchComplexDemo {

   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      while (true) {

         System.out.println("Enter your regex: ");
         String pstring = in.nextLine();
         System.out.println("Enter input string to search: ");
         String mstring = in.nextLine();

         Pattern pattern = Pattern.compile(pstring);
         Matcher matcher = pattern.matcher(mstring);

         boolean found = false;
         while (matcher.find()) {
            System.out.printf(
                  "I found the text \"%s\" starting at index %d and ending "
                        + "at index %d.%n", matcher.group(), matcher.start(),
                  matcher.end());
            found = true;
         }

         if (!found) 
            System.out.println("No match found.%n");
      }
   }
}
