package school.lesson4.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceComplexDemo {


      public static void main(String[] args) {

         Scanner in = new Scanner(System.in);

         while (true) {

            System.out.println("Enter your regex: ");
            String pstring = in.nextLine();
            System.out.println("Enter input string to search: ");
            String mstring = in.nextLine();
            System.out.println("Enter replacement string: ");
            String rstring = in.nextLine();
            
            Pattern pattern = Pattern.compile(pstring);
            Matcher matcher = pattern.matcher(mstring);
            
            String replaced = matcher.replaceAll(rstring);
 
            System.out.println(replaced);
      }
   }
}