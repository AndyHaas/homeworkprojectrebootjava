package school.lesson4.regex;

import java.util.regex.Pattern;

public class IPAdressCheckDemo {

   public static void main(String[] args) {

   
        final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
   
   //   final String IPADDRESS_PATTERN_SHORT = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])"
   //         + "(\\.[01]?\\d\\d?|2[0-4]\\d|25[0-5]){3}";
      
      String[] ips = { "255.255.255.255", "0.0.0.0", "255,255.255.255",
            "127.0.0.1", "192.168.0.1", "355.255.255.255","192.168.0.1.2" };

      for (String ip : ips) {

         if (Pattern.matches(IPADDRESS_PATTERN, ip)) {
            System.out.println(ip + " is a valid IP address");
         } else {
            System.out.println(ip + " is not a valid IP address");
         }
      }
   }
}
