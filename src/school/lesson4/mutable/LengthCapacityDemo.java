package school.lesson4.mutable;

public class LengthCapacityDemo {

   public static void main(String[] args) {

      StringBuilder sb = new StringBuilder("Hello world!");

      System.out.println("buffer = " + sb);
      System.out.println("length = " + sb.length());
      System.out.println("capacity = " + sb.capacity());
   }
}