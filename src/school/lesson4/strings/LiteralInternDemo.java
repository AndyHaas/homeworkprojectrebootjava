package school.lesson4.strings;

public class LiteralInternDemo {
   
   public static void main(String[] args) {
      
      String greeting = "Hello";
      String hello = "Hello";
      
      System.out.println("First string: " + greeting);
      System.out.println("Second string: " + hello);
      
      System.out.println();
            
      System.out.println("References are equal : " + (greeting == hello));
      System.out.println("The same character sequence : " + greeting.equals(hello));

   }
}