package school.lesson4.strings;

public class ConcatDemo {
 
   public static void main(String[] args) {
      
      String hello = "Hello";
      String space = " ";
      String world = "World";
      String exclamation = "!";
      
      String helloworld = "Hello World!";
      String result = hello.concat(space).concat(world).concat(exclamation);
     
      System.out.println(result);
      System.out.println(helloworld);
      System.out.println("Equal references ? " + (result == helloworld));
      System.out.println("Have equal contents ? " + result.equals(helloworld));
    }
}
