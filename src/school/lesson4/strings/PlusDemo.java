package school.lesson4.strings;

public class PlusDemo {
 
   public static void main(String[] args) {
      
      String helloworldplus = "Hello" + " " + "World" + "!";
      String helloworld = "Hello World!";
      
      System.out.println(helloworldplus);
      System.out.println(helloworld);
      System.out.println("Equal references ? " + (helloworldplus == helloworld));
      System.out.println("Have equal contents ? " + helloworldplus.equals(helloworld));
    }
}
