package school.lesson4.strings;

public class InternDemo {
	
   public static void main(String[] args) {
      
      String greeting = new String("Hello");
      String hello = "Hello";
      
      System.out.println("First string: " + greeting);
      System.out.println("Second string: " + hello);
      System.out.println();
      
      System.out.println("References are equal : " + (greeting == hello));
      System.out.println("The same character sequence : " + greeting.equals(hello));
      
      greeting = greeting.intern();
      System.out.println("\n Interning string created using constructor! \n");
      
   
      System.out.println("References are equal : " + (greeting == hello));
      System.out.println("The same character sequence : " + greeting.equals(hello));
    }
}
