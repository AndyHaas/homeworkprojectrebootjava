package school.lesson4.strings;

public class CompareDemo {

   public static void main(String[] args) {

      String apple = "apple";
      String orange = "orange";
      
      System.out.println("Comparing orange to apple: " + orange.compareTo(apple));
      System.out.println("Comparing apple to orange: " + apple.compareTo(orange));
      System.out.println("Comparing apple to apple: " + apple.compareTo(apple));
      System.out.println("Comparing orange to orange: " + orange.compareTo(orange));
   }
}
