package school.lesson3.abstracts;

public class Vet {

   public Vet(String name) {

      this.name = name;
   }

   public void makeShots(Animal[] animals) {

      System.out.println("Hello my name is " + name
            + " I am an animal doctor \nI am here to vaccinate animals.....\n");
      for (Animal animal : animals) {
         System.out.println(animal.getName() + ": " + animal.makeNoise());
      }
   }

   String name;
}