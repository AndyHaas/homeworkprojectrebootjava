package school.lesson3.abstracts;

public class ApplianceChecker {

   public static void checkAppliances(Appliance[] apps, int maxPower) {

      int totalPower = 0;

      System.out.println("Maximum power consumption: " + maxPower + "\n");
      
      for (Appliance app : apps) {

         if (app.isOn()) {
            if (maxPower < totalPower + app.getPower()) {
               app.turnOff();
               System.out.println("switching " + app.getDescription() + " OFF");
            } else {

               System.out.println(app.getDescription() + " is ON consuming "
                     + app.getPower() + " watts");
               totalPower += app.getPower();
            }
         } else
            System.out.println(app.getDescription() + " is OFF");
      }

      System.out.println("\nTotal power consumption: " + totalPower);
   }
}
