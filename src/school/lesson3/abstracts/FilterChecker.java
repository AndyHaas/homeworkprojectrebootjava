package school.lesson3.abstracts;

public class FilterChecker {

   public static void check(String text, Filter head) {

      Filter filter = head;

      while (filter != null) {

         if (!filter.isOk(text))
            return;

         filter = filter.getNext();
      }
      System.out.println("email is ok!");
   }
}
