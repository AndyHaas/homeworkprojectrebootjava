package school.lesson3.obectmeth;

public class CloneDemo {

   public static void main(String[] args) {
      try {
         Employee original = new Employee("Tonny Tester", 20000, 2000, 10, 23);
         original.setHireDay(2000, 1, 1);
         Employee copy = original.clone();

         System.out.println("original=" + original); 
         System.out.println("copy=" + copy);
         
         System.out.println("changing copy now ...");
         
         copy.raiseSalary(10);
         copy.setHireDay(2002, 12, 31);
                 
         System.out.println("original=" + original);
         System.out.println("copy=" + copy);
      
         
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
      }
   }
}
