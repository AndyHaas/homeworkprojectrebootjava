package school.lesson3.obectmeth;

public class ToStringDemo {
  
   public static void main(String[] args) {
      
      Manager boss = new Manager("Carl Cracker", 30000, 1987, 12, 15);
      boss.setBonus(5000);

      Object[] objects  = new Object[3];

      objects [0] = boss;
      objects [1] = new Employee("Harry Hacker", 25000, 1989, 10, 1);
      objects [2] = new Employee("Tommy Tester", 20000, 1990, 3, 15);

      // toString is not necessary
      
      for (Object o : objects)
         System.out.println(o.toString());
   }
}
