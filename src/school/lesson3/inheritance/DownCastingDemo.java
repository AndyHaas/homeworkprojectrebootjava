package school.lesson3.inheritance;

public class DownCastingDemo {

   public static void main(String[] args) {

      Person person = new Manager("Robert", 30000, 5);

      System.out.println("\nA person: name = " + person.getName());

      System.out.println("\nI can cast to Employee: "
            + (person instanceof Employee));
      Employee employee = (Employee) person;
      System.out.println("An employee: name = " + employee.getName()
            + ", salary = " + employee.getSalary());

      System.out.println("\nI can cast to Manager: "
            + (employee instanceof Manager));
      Manager manager = (Manager) employee;
      System.out.println("A manager: name = " + manager.getName()
            + ", salary = " + manager.getSalary() + ", reports = "
            + manager.getReports());
   }
}
