package school.lesson3.inheritance;

class DescriptionPrinter {
   
   public static void printDescriptions(Person[] people) {

      for (Person person : people) {
         System.out.println(getDescription(person));
      }
   }
   
   private static String getDescription(Person person) {

      if (person instanceof Manager) {

         Manager manager = (Manager) person;
         return "A manager: name = " + manager.getName() + ", salary = "
               + manager.getSalary() + ", direct reports = " + manager.getReports();
      }

      if (person instanceof Employee) {

         Employee employee = (Employee) person;
         return "An employee: name = " + employee.getName() + ", salary = "
               + employee.getSalary();
      }
      return "A person: name = " + person.getName();
   }
}

public class NoPolymorphismDemo {
   
   public static void main(String[] args) {

      Person[] people = new Person[3];

      people[0] = new Person("John Whatson");
      people[1] = new Employee("Harry Hacker", 25000);
      people[2] = new Manager("Carl Cracker", 30000.00, 7);

      System.out.println();
      DescriptionPrinter.printDescriptions(people);
   }
}
