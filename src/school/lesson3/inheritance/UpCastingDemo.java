package school.lesson3.inheritance;

public class UpCastingDemo {

   public static void main(String[] args) {

      Manager manager = new Manager();
      manager.setName("Robert");
      manager.setSalary(30000);
      manager.setReports(5);
      System.out.println("A manager: name = " + manager.getName()
            + ", salary = " + manager.getSalary() + ", reports = "
            + manager.getReports());

      Employee employee = manager;
      System.out.println("\nAn employee: name = " + employee.getName()
            + ", salary = " + employee.getSalary());
      System.out.println("Am I still a manager: "
            + (employee instanceof Manager));
      
      
      Person person = employee;
      System.out.println("\nA person: name = " + person.getName());
      System.out.println("Am I still a manager: "
            + (person instanceof Manager));

   }
}
