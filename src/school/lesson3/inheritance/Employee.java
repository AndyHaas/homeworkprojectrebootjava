package school.lesson3.inheritance;

class Employee extends Person {

   public Employee() {
   }
   
   public Employee(String name, double salary) {
      super(name);
      this.salary = salary;
      System.out.println("Employee constructor is called setting salary = " + salary);
   }

   public double getSalary() {
      return salary;
   }

   public void setSalary(double salary) {
      this.salary = salary;
   }

   public void raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
   }

   
   public String getDescription() {
      
      return "An employee: name = " + getName() + ", salary = "
            + getSalary();
   }
   
   /*
   public String getDescription() {
      return super.getDescription() + "[salary = " + getSalary()+ "]";
   }
   */
   
   private double salary;
}