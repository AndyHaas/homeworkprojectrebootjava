package school.lesson3.polymorphism;

class Employee extends Person{
   
   public Employee(String name, double salary) {
      super(name);
      this.salary = salary;
   }

   public double getSalary() {
      return salary;
   }

   public void setSalary(double salary) {
      this.salary = salary;
   }

   public void raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
   }
   
   public String getDescription() {
      
      return "An employee: name = " + getName() + ", salary = "
            + getSalary();
   }
   
   private double salary;
}