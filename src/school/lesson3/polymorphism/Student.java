package school.lesson3.polymorphism;

class Student extends Person {

   public Student(String name, String major) {
      super(name);
      this.major = major;
   }

   public String getMajor() {
      return major;
   }

   public void setMajor(String major) {
      this.major = major;
   }

   public String getDescription() {

      return "A student: name = " + getName() + ", major = " + getMajor();
   }

   private String major;
}
