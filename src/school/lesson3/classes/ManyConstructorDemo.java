package school.lesson3.classes;


public class ManyConstructorDemo {
   public static void main(String[] args) {

      ManyConstructorEmployee[] staff = new ManyConstructorEmployee[3];

      staff[0] = new ManyConstructorEmployee("Harry", 40000);
      staff[1] = new ManyConstructorEmployee(60000);
      staff[2] = new ManyConstructorEmployee();

      for (ManyConstructorEmployee e : staff)
         System.out.println("name=" + e.getName() + ",id=" + e.getId()
               + ",salary=" + e.getSalary());
   }
}

class ManyConstructorEmployee {
   public ManyConstructorEmployee(String n, double s) {
      name = n;
      salary = s;
   }

   public ManyConstructorEmployee(double s) {
      this("Employee #" + nextId, s);
   }

   public ManyConstructorEmployee() {

   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public int getId() {
      return id;
   }

   private static int nextId = 1;

   private int id;
   private String name = "";
   private double salary;
  
   {
      id = nextId;
      nextId++;
   }
}