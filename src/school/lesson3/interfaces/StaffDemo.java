package school.lesson3.interfaces;

public class StaffDemo {

   public static void main(String[] args) {
      
      Employee [] emps = new Employee[5];

      emps[0] = new Employee("Harry Hacker", 30000);
      emps[1] = new Employee("Tommy Tester", 17000);
      emps[2] = new Salesman("Ed Sell", 25000, 4000);
      emps[3] = new Employee("Alice Coder", 29000);
      emps[4] = new Salesman("Bobby Seller", 30000, 10000);
      
      String statement = Staff.getStatement(emps);
      
      System.out.println(statement);
   }
}
