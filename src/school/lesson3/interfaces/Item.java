package school.lesson3.interfaces;

public class Item implements Comparable {
   
   public Item(String name, int number) {
      this.name = name;
      this.number = number;
   }

   public String toString() {
      return "[name=" + name + ", number=" + number + "]";
   }
   
   public int compareTo(Comparable other) {
    
      Item otherItem = (Item)other;
      return (number > otherItem.number ? 1 :(number < otherItem.number ? -1: 0));
   }
   
   private String name;
   private int number;
}
