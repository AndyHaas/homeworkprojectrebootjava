package lesson4.strings;

/*
Write a program that reads a number between 1,000 and 999,999 from the user,
where the user enters a comma in the input. Then print the number without a
comma. Here is a sample dialog; the user input is in color:
Please enter an integer between 1,000 and 999,999: 23,456 23456
Hint: Read the input as a string. Measure the length of the string. Suppose it contains
n characters. Then extract substrings consisting of the first n – 4 characters and the
last three characters.

Напишите программу, которая читает число от 1000 до 999 999 от пользователя, где пользователь вводит запятую в поле ввода.
Затем напечатайте номер без запятой. Вот пример диалога; Введенный пользователем цвет: пожалуйста, введите целое число
от 1000 до 999 999: 23 456 23456
Подсказка: прочитайте ввод как строку. Измерьте длину строки. Предположим, он содержит n символов.
Затем извлеките подстроки, состоящие из первых n - 4 символов и последние три символа.
*/

import java.util.Scanner;

class P2_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter an integer between 1,000 and 999,999: ");
        String input = scanner.nextLine();

        String left = input.substring(0, input.length() - 4);
        String right = input.substring(input.length() - 3);

        System.out.println(left + "," + right);
    }
}
