package lesson4.strings;

/*
Write a program that prints a ­Christmas tree:
   /\'
  /  \'
 /    \'
/      \'
--------
  "  "
  "  "
  "  "
Remember to use escape sequences.

Напишите программу, которая печатает елку:
Не забудьте использовать escape-последовательности.
*/

import java.util.stream.IntStream;

class P2_20 {
    private static final String LINE1 = "   /\\'";
    private static final String LINE2 = "  /  \\'";
    private static final String LINE3 = " /    \\'";
    private static final String LINE4 = "/      \\'";
    private static final String LINE = "--------\n";
    private static final String BASE = "  \"  \"\n";

    public static void main(String[] args) {
        printChristmasTree();
    }

    private static void printChristmasTree() {
        System.out.println(LINE1);
        System.out.println(LINE2);
        System.out.println(LINE3);
        System.out.println(LINE4);
        System.out.print(LINE);
        IntStream.range(0, 3).mapToObj(i -> BASE).forEach(System.out::print);
    }
}