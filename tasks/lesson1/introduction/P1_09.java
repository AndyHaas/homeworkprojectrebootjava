package lesson1.introduction;

/*Write a program that prints a house that looks exactly like the following:
   +
  + +
 +   +
+-----+
| .-. |
| | | |
+-+-+-+
*/
class P1_09 {
    public static void main(String[] args) {
        System.out.println("          /\\        ");
        System.out.println("        /    \\      ");
        System.out.println("      /        \\    ");
        System.out.println("    ++++++++++++++   ");
        System.out.println("    |   ------   |   ");
        System.out.println("    |   |    |   |   ");
        System.out.println("    |   |    |   |   ");
        System.out.println("    ++++++++++++++   ");
    }
}

