package lesson1.introduction;

/*
Write a program that displays your name inside a box on the screen, like this: Dave
Do your best to approximate lines with characters such as |- + .
*/

import java.util.Scanner;

class P1_05 {

    private static final String VERTICAL_BORDER = "|";

    public static void main(String[] args) {
        String name = getUserName();

        setHorizontalLineToSize(name);
        addVerticalNameBorder(name);
        setHorizontalLineToSize(name);
    }

    private static void addVerticalNameBorder(String name) {
        System.out.println(VERTICAL_BORDER + name + VERTICAL_BORDER);
    }

    private static void setHorizontalLineToSize(String name) {
        System.out.print("+");

        for (int i = 0; i < name.length(); i++) {
            System.out.print("-");
        }

        System.out.println("+");
    }

    private static String getUserName() {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите ваше имя: ");
        return input.nextLine();
    }
}
