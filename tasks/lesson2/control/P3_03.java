package lesson2.control;

/*Write a program that reads an integer and prints how many digits the number has, by
checking whether the number is ≥ 10, ≥ 100, and so on. (Assume that all integers are
less than ten billion.) If the number is negative, first multiply it with –1.*/

import java.util.Scanner;

class P3_03 {
    public static void main(String[] args) {
        int value = getValue();
        getValueLength(value);
    }

    private static void getValueLength(int value) {
        value = Math.abs(value);
        String stringValue = String.valueOf(value);
        int digitCount = stringValue.length();
        System.out.print("Количество цифр во введённом числе: " + digitCount + " единиц.");
    }

    private static int getValue() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите целочисленное значение: ");
        return in.nextInt();
    }
}