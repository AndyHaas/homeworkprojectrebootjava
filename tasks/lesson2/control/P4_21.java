package lesson2.control;

/*Write a program that reads an integer and displays, using asterisks, a filled diamond
of the given side length. For example, if the side length is 4, the program should display

Напишите программу, которая читает целое число и отображает, используя звездочки,
заполненный ромб с заданной длиной стороны. Например, если длина стороны равна 4, программа должна отобразить
     *
    ***
   *****
  *******
   *****
    ***
     *
 */

import java.util.Scanner;

class P4_21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите значение величины стороны ромба: ");
        int input = scanner.nextInt();

        for (int i = -input; i <= input; i++) {
            for (int j = -input; j <= input; j++) {
                System.out.print(Math.abs(i) + Math.abs(j) < input ? "*" : " ");
            }
            System.out.println();
        }
    }
}