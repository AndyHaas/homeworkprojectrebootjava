package lesson2.control;

/*A year with 366 days is called a leap year. Leap years are necessary to keep the calendar
synchronized with the sun because the earth revolves around the sun once
every 365.25 days. Actually, that figure is not entirely precise, and for all dates after
1582 the Gregorian correction applies. Usually years that are divisible by 4 are leap
years, for example 1996. However, years that are divisible by 100 (for example, 1900)
are not leap years, but years that are divisible by 400 are leap years (for exam­ple,
2000). Write a program that asks the user for a year and computes whether that year
is a leap year. Use a single if statement and Boolean operators.

Год с 366 днями называется високосным. Високосные годы необходимы для синхронизации календаря с Солнцем,
потому что Земля вращается вокруг Солнца один раз каждые 365,25 дней. На самом деле эта цифра не совсем точна,
и для всех дат после 1582 года применяется григорианская поправка. Обычно годы, которые делятся на 4,
являются високосными, например - 1996. Однако годы, которые делятся на 100 (например, 1900), не являются високосными,
а годы, которые делятся на 400, являются високосными (например, 2000).
Напишите программу, которая запрашивает у пользователя год и вычисляет, является ли этот год високосным.
Используйте один оператор if и логические операторы.*/

import java.util.Calendar;
import java.util.Scanner;

class P3_28 {
    public static void main(String[] args) {
        int year = getYear();
        verify(year);
    }

    private static void verify(int year) {
        System.out.println(!isLeapYear(year) ? "Not leap year" : "Leap year");
    }

    private static int getYear() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter year: ");
        return scanner.nextInt();
    }

    private static boolean isLeapYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        return calendar.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }
}