package lesson2.control;

/*Write a program that reads three numbers and prints “increasing” if they are in
increasing order, “decreasing” if they are in decreasing order, and “neither” other­
wise. Here, “increasing” means “strictly increasing”, with each value larger than its
pre­depressor. The sequence 3 4 4 would not be considered increasing.*/

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

class P3_05 {
    public static void main(String[] args) {
        int[] array = initArray();
        verify(array);
    }

    private static void verify(int[] array) {
        if (array[0] <= array[1] && array[1] <= array[2]) {
            System.out.println("increasing");
        } else if (array[0] >= array[1] && array[1] >= array[2]) {
            System.out.println("decreasing");
        } else {
            System.out.println("neither");
        }
    }

    private static int[] initArray() {
        Random rnd = new Random();
        int[] array = IntStream.range(0, 3).map(i -> rnd.nextInt(10)).toArray();
        System.out.println("Original array is: " + Arrays.toString(array));
        return array;
    }
}