package lesson2.arrays;

/*Write a method that reverses the sequence of elements in an array. For example, if
you call the method with the array
1  4  9  16  9  7  4  9  11
then the array is changed to
11  9  4  7  9  16  9  4  1*/

import java.util.Arrays;

class P6_07 {
    public static void main(String[] args) {
        int[] array = P6_06.initArray(10, 50);
        reverseArray(array);
    }

    private static void reverseArray(int[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }
}
