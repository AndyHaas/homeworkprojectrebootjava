package lesson2.arrays;

/*Write a method
public static boolean sameSet(int[] a, int[] b)
that checks whether two arrays have the same elements in some order, ignoring
duplicates. For example, the two arrays
1  4  9  16  9  7  4  9  11
and
11  11  7  9  16  4  1
would be considered identical. You will probably need one or more helper methods.*/

import java.util.Arrays;
import java.util.Scanner;

class P6_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] firstArray = initArray(input, 5);

        int[] secondArray = initArray(input, 5);

        boolean isSameSets = isSameSet(firstArray, secondArray);
        System.out.printf("Это тот же массив?: %s", isSameSets);
    }

    private static int[] initArray(Scanner input, int arrayLength) {
        int[] firstArray = new int[arrayLength];
        for (int i = 0; i < firstArray.length; i++) {
            System.out.printf("%d Введите значение для помещения в массив: ", i + 1);
            firstArray[i] = input.nextInt();
        }
        return firstArray;
    }

    private static boolean isSameSet(int[] a, int[] b) {

        boolean isSameSet = false;

        for (int item : a) {
            if (Arrays.stream(b).anyMatch(value -> item == value)) {
                isSameSet = true;
            }

            if (!isSameSet) {
                return false;
            }
        }

        return true;
    }
}