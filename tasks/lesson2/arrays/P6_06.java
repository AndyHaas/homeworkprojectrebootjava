package lesson2.arrays;

/*Compute the alternating sum of all elements in an array. For example, if your program
reads the input then it computes
1  4  9  16  9  7  4  9  11
1 – 4 + 9 – 16 + 9 – 7 + 4 – 9 + 11 = –2*/

import java.util.Arrays;
import java.util.Random;

class P6_06 {
    public static void main(String[] args) {
        int[] array = initArray(10, 50);
        System.out.println("Alternating sum of all elements: " + calcAlternatingSum(array));
    }

    static int[] initArray(final int arrayLength, final int bound) {
        int[] array = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }
        System.out.println("Original array: " + Arrays.toString(array));
        return array;
    }

    private static int calcAlternatingSum(int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += i % 2 == 0 ? array[i] : -array[i];
        }

        return sum;
    }
}