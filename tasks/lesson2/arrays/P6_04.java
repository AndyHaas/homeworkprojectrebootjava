package lesson2.arrays;

/*Write a method sumWithoutSmallest that computes the sum of an array of values,
except for the smallest one, in a single loop. In the loop, update the sum and the
smallest value. After the loop, return the difference.*/

import java.util.Arrays;
import java.util.Random;

public class P6_04 {
    public static void main(String[] args) {
        P6_04 p6_04 = new P6_04();
        p6_04.sumWithoutSmallest();
    }

    private void sumWithoutSmallest() {
        int[] array = new int[10];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int smallest = array[0];
        int sum = 0;

        for (int value : array) {
            sum += value;
            if (value < smallest) {
                smallest = value;
            }
        }
        sum -= smallest;

        System.out.println("Original array: " + Arrays.toString(array));

        System.out.println("Sum of an array of values without smallest: " + sum);

    }
}
