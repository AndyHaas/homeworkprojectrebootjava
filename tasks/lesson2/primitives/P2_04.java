package lesson2.primitives;

/*
 Write a program that prompts the user for two integers and then prints
 •    The sum
 •    The difference
 •    The product
 •    The average
 •    The distance (absolute value of the difference)
 •    The maximum (the larger of the two)
 •    The minimum (the smaller of the two)
 Hint: The max and min functions are declared in the Math class.
*/

import java.util.Scanner;

class P2_04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое целое число: ");
        int firstInt = scanner.nextInt();
        System.out.println("Введите вторвое целое число: ");
        int secondInt = scanner.nextInt();

        System.out.printf("The sum is: %d %n", firstInt + secondInt);
        System.out.printf("The difference: %d %n", firstInt - secondInt);
        System.out.printf("The product(composition): %d %n", firstInt * secondInt);
        System.out.printf("The average: %.2f %n", (float) (firstInt + secondInt) / 2);
        System.out.printf("The distance: %d %n", Math.abs(firstInt - secondInt));
        System.out.printf("The maximum: %d %n", Math.max(firstInt, secondInt));
        System.out.printf("The minimum: %d %n", Math.min(firstInt, secondInt));
    }
}