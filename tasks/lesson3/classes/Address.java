package lesson3.classes;

/*Implement a class Address . An address has a house number, a street, an optional
apartment number, a city, a state, and a postal code. Supply two constructors: one
with an apartment number and one without. Supply a print method that prints the
address with the street on one line and the city, state, and zip code on the next line.
Supply a method public boolean comesBefore(Address other) that tests whether this
address comes before another when the addresses are compared by postal code.

Реализуйте класс Address. Адрес имеет номер дома, улицу, необязательный номер квартиры, город, штат и почтовый индекс.
Создайте два конструктора: один с номером квартиры и один без.
Укажите метод печати, который печатает адрес с улицей на одной строке, а город, штат и почтовый индекс на следующей строке.
Укажите метод public boolean comeBefore (Address other), который проверяет, предшествует ли этот адрес другому,
когда адреса сравниваются по почтовому индексу.
*/

class Address {

    private int houseNumber;
    private String street;
    private int apartmentNumber;
    private String city;
    private int zipCode;

    private Address(int zipCode, String city, String street, int houseNumber) {
        this.houseNumber = houseNumber;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
    }

    private Address(int zipCode, String city, String street, int houseNumber, int apartmentNumber) {
        this.zipCode = zipCode;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
    }

    private void printAddress() {
        System.out.printf("ул.%s, д.%s, оф.%s\n", this.street, this.houseNumber, this.apartmentNumber);
        System.out.printf("г.%s, %d\n", this.city, this.zipCode);
    }

    private boolean comeBefore(Address other) {
        return this.zipCode < other.zipCode;
    }

    public static void main(String[] args) {
        Address secondAddress = new Address(630015, "Новосибирск", "Семьи Шамшиных", 64);
        Address lastAddress = new Address(630049, "Новосибирск", "Советская", 5, 4);
        secondAddress.printAddress();
        System.out.println("Address come before: " + secondAddress.comeBefore(lastAddress));
    }
}
